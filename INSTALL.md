# deploy_chat.yaml

### requirements : 

- an new *empty* server (not responsible of damages if you run this on a server with stuff on it already) with one of the following OS :
  - Ubuntu 20.04 LTS x64
  - Ubuntu 22.04 LTS x64
  - Debian 11 x64

- DNS
  - if you want to use your own domain name and deploy in single room mode, you will have to create an A record that points to the ip of your server.
  - if you want to use your own domain name and deploy in multi-room mode, you will have to create an A record and a [wildcard dns record](https://en.wikipedia.org/wiki/Wildcard_DNS_record) that points to the ip of your server.

### deployment options :

- multirooms
  - `true` : will create a webpage where you can create random chatrooms (like temporary.chat)
  - `false` : will create a webpage where you can create a single chatroom
- tls
  - `wildcard` : run this mode if you want to run a multi room setup able to create more than 50 chatrooms per week (will use let's encrypt)
    - only useful when `multirooms:true`
    - requires an interaction during the deployment
      - you will need to add a TXT entry in your dns records (the playbook will give you this information while running)
  - `normal` : run this mode if you intend to create 50 chatrooms per week maximum (will use let's encrypt)
    - when used with `multirooms:true`
      - requires no interaction during deployment
      - rooms takes a bit longer to create (one certificate is issued per room)
    - when used with `multirooms:false`
      - you can use this to create a single chatroom with dynamic dns or with your fully qualified domain name.
- `pki` : run this mode if you don't have a fully qualified domain 
  - does not require anything
  - will automatically create a self sign certificate 
  - will create a helper webpage to help you download/install the certificate 
    - the fingerprint of the certificate will be displayed on the helper webpage

## deployment examples :
### deploy with a fully qualified domain name that you own in multirooms mode 
##### usage example :
`ansible-playbook -i inventory -e '{"have_fqdn":true}' -e '{"multirooms":true}' -e "my_fqdn=yourhostname.com" -e "tls_mode=wildcard" -e "le_mailaddress=all@200013.net" deploy_chat.yaml`

or

`ansible-playbook -i inventory -l chat_with_hostname -e '{"have_fqdn":true}' -e '{"multirooms":true}' -e "my_fqdn=yourhostname.com" -e "tls_mode=normal" -e "le_mailaddress=all@200013.net" deploy_chat.yaml`
### deploy with a fully qualified domain or subdomain name that you own (single room)
##### usage example : 
`ansible-playbook -i inventory -l chat_with_hostname  -e '{"have_fqdn":true}' -e '{"multirooms":false}' -e "my_fqdn=yourhostname.com" -e "tls_mode=normal" -e "le_mailaddress=all@200013.net" deploy_chat.yaml`

### deploy *without* a fully qualified domain name (single room accessible via ip)
##### usage example : 
`ansible-playbook -i inventory -l chat_with_ip -e '{"have_fqdn":false}' -e '{"multirooms":false}' -e "my_fqdn=192.168.1.12" -e "tls_mode=pki" deploy_chat.yaml`


*** -  this repository is mirrored from gitea ***
