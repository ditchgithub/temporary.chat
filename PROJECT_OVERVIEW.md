### Overview of the project : 
an ansible playbook to deploy a chat server.
- works with or without a domain name
- can create a single room or multiples (random) rooms 
- you can create password protected rooms
- you can upload and share files (with preview for png, jpg and gif)
- you can subscribe to an rss feed to receive all updates of a room (or only when someone pings you)
- receive desktop notifications on new messages when someone pings you (not for mobile)
- anyone within the room can destroy it with a single click

[Screenshot1](https://user-images.githubusercontent.com/70525822/108941864-d4241700-7623-11eb-9d6e-c8ccd8af5689.png)

[Screenshot2](https://user-images.githubusercontent.com/70525822/108941906-ec943180-7623-11eb-8a50-f2f232cb80a0.png)

[Screenshot3](https://user-images.githubusercontent.com/70525822/108942231-9e336280-7624-11eb-83e5-234e48424c61.png)

[Screenshot4](https://user-images.githubusercontent.com/70525822/108942290-b905d700-7624-11eb-86a9-32fe4de12c38.png)

[Screenshot5](https://user-images.githubusercontent.com/70525822/108942390-e81c4880-7624-11eb-9500-6015d0625524.png)

[Screenshot6](https://user-images.githubusercontent.com/70525822/108942396-eb173900-7624-11eb-8af7-74788d77e34d.png)
